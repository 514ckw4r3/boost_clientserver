#pragma once

#include <memory>
#include <boost/asio.hpp>

class BaseCommand
{
public:
	explicit BaseCommand(boost::asio::ip::tcp::socket &socket);
	virtual ~BaseCommand() {}

	void Write(std::string &sData) const;

	virtual void Execute() = 0;
	virtual std::string Name() const = 0;

private:
	boost::asio::ip::tcp::socket &m_socket;
};

typedef std::shared_ptr<BaseCommand> CommandPtr;