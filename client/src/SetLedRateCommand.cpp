#include "SetLedRateCommand.h"

#include <iostream>

static const std::string sName = "set-led-rate";

////////////////////////////////////////////////////////////////////////////////

SetLedRateCommand::SetLedRateCommand(boost::asio::ip::tcp::socket &socket)
	: BaseCommand(socket)
{

}

////////////////////////////////////////////////////////////////////////////////

void SetLedRateCommand::Execute()
{
	std::string sCommand;

	bool bValid = false;
	do
	{
		std::cout << "\t enter a rate from 0 to 5\n";
		int rate;
		std::cin >> rate;

		if (rate >= 0 && rate <= 5)
		{
			bValid = true;
			sCommand = sName + ' ' +  std::to_string(rate);
		}

	} while (!bValid);

	Write(sCommand);
}

////////////////////////////////////////////////////////////////////////////////

std::string SetLedRateCommand::Name() const
{
	return sName;
}

////////////////////////////////////////////////////////////////////////////////
