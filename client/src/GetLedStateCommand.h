#pragma once

#include <boost/asio.hpp>
#include "BaseCommand.h"

class GetLedStateCommand : public BaseCommand
{
public:
	explicit GetLedStateCommand(boost::asio::ip::tcp::socket &socket);
	virtual void Execute() override;
	virtual std::string Name() const override;
};