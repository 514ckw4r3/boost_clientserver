#pragma once

#include <boost/asio.hpp>
#include "BaseCommand.h"

class GetLedRateCommand : public BaseCommand
{
public:
	explicit GetLedRateCommand(boost::asio::ip::tcp::socket &socket);
	virtual void Execute() override;
	virtual std::string Name() const override;
};