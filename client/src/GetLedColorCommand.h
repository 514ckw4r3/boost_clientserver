#pragma once

#include <boost/asio.hpp>
#include "BaseCommand.h"

class GetLedColorCommand : public BaseCommand
{
public:
	explicit GetLedColorCommand(boost::asio::ip::tcp::socket &socket);
	virtual void Execute() override;
	virtual std::string Name() const override;
};