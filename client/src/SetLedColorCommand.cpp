#include "SetLedColorCommand.h"

#include <iostream>

static const std::string sName = "set-led-color";

////////////////////////////////////////////////////////////////////////////////

SetLedColorCommand::SetLedColorCommand(boost::asio::ip::tcp::socket &socket)
	: BaseCommand(socket)
{

}

////////////////////////////////////////////////////////////////////////////////

void SetLedColorCommand::Execute()
{
	std::string sCommand;

	bool bValid = false;
	do
	{
		std::cout << "\t 0) red \n\t 1) green \n\t 2) blue \n";
		int color;
		std::cin >> color;

		if (color == 0 || color <= 2)
		{
			bValid = true;

			std::string sColor = "red";
			if (color == 1) sColor = "green";
			else if (color == 2) sColor = "blue";

			sCommand = sName + ' ' +  sColor;
		}

	} while (!bValid);

	Write(sCommand);
}

////////////////////////////////////////////////////////////////////////////////

std::string SetLedColorCommand::Name() const
{
	return sName;
}

////////////////////////////////////////////////////////////////////////////////
