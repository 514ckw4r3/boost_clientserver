#pragma once

#include <boost/asio.hpp>
#include "BaseCommand.h"

class SetLedColorCommand : public BaseCommand
{
public:
	explicit SetLedColorCommand(boost::asio::ip::tcp::socket &socket);
	virtual void Execute() override;
	virtual std::string Name() const override;
};
