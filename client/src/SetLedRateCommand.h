#pragma once

#include <boost/asio.hpp>
#include "BaseCommand.h"

class SetLedRateCommand : public BaseCommand
{
public:
	explicit SetLedRateCommand(boost::asio::ip::tcp::socket &socket);
	virtual void Execute() override;
	virtual std::string Name() const override;
};
