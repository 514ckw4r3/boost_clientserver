#pragma once

#include <boost/asio.hpp>
#include "BaseCommand.h"

class SetLedStateCommand : public BaseCommand
{
public:
	explicit SetLedStateCommand(boost::asio::ip::tcp::socket &socket);
	virtual void Execute() override;
	virtual std::string Name() const override;
};
