#include "SetLedStateCommand.h"

#include <iostream>

static const std::string sName = "set-led-state";

////////////////////////////////////////////////////////////////////////////////

SetLedStateCommand::SetLedStateCommand(boost::asio::ip::tcp::socket &socket)
	: BaseCommand(socket)
{

}

////////////////////////////////////////////////////////////////////////////////

void SetLedStateCommand::Execute()
{
	std::string sCommand;

	bool bValid = false;
	do
	{
		std::cout << "\t 0) on \n\t 1) off\n";
		int state;
		std::cin >> state;

		if (state == 0 || state == 1)
		{
			bValid = true;
			sCommand = sName + ' ' + (!state ? "on" : "off");
		}

	} while (!bValid);

	Write(sCommand);
}

////////////////////////////////////////////////////////////////////////////////

std::string SetLedStateCommand::Name() const
{
	return sName;
}

////////////////////////////////////////////////////////////////////////////////
