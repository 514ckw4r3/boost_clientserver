#include <cstdlib>
#include <cstring>
#include <iostream>
#include <boost/asio.hpp>
#include <array>
#include <vector>

#include "SetLedStateCommand.h"
#include "GetLedStateCommand.h"
#include "SetLedColorCommand.h"
#include "GetLedColorCommand.h"
#include "SetLedRateCommand.h"
#include "GetLedRateCommand.h"

using boost::asio::ip::tcp;

std::vector<CommandPtr> m_commands;

////////////////////////////////////////////////////////////////////////////////

void ShowMenu()
{
	std::cout << "====================================\n\n"
			  << "Commands:"
			  << '\n';

	int i = 0;
	for (const CommandPtr &pCommand : m_commands)
	{
		std::cout << i++ << ") "
				  << pCommand->Name()
				  << '\n';
	}
	std::cout << i << ") "
			  << "exit"
			  << '\n';
}

////////////////////////////////////////////////////////////////////////////////

int main(int argc, char **argv)
{
	try
	{
		boost::asio::io_context io_context;

		tcp::socket socket(io_context);
		tcp::resolver resolver(io_context);

		std::string host = "127.0.0.1";
		std::string port = "80";

		if (argc == 3)
		{
			host = argv[1];
			port = argv[2];
		}

		boost::asio::connect(socket, resolver.resolve(host, port));

		m_commands.emplace_back(std::make_shared<SetLedStateCommand>(socket));
		m_commands.emplace_back(std::make_shared<GetLedStateCommand>(socket));
		m_commands.emplace_back(std::make_shared<SetLedColorCommand>(socket));
		m_commands.emplace_back(std::make_shared<GetLedColorCommand>(socket));
		m_commands.emplace_back(std::make_shared<SetLedRateCommand>(socket));
		m_commands.emplace_back(std::make_shared<GetLedRateCommand>(socket));

		bool bExit = false;
		do
		{
			ShowMenu();
			int command;
			std::cin >> command;

			if (command < 0 || command >= m_commands.size())
			{
				if (command == m_commands.size())
				{
					bExit = true;
				}
				else
				{
					std::cout << "Invalid command\n";
				}
			}
			else
			{
				auto pCommand = m_commands[command];
				pCommand->Execute();

				boost::asio::streambuf buf;
				boost::asio::read_until(socket, buf, "\n");
				std::cout << "Command result: " << &buf << '\n';
			}
		} while (!bExit);
	}
	catch (const std::exception &e)
	{
		std::cerr << "Caught: " << e.what() << '\n';
	}
}

////////////////////////////////////////////////////////////////////////////////
