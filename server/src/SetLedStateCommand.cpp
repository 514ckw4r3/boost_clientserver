#include "SetLedStateCommand.h"

#include <regex>
#include <sstream>
#include "Logger.h"
#include "Led.h"
#include "Utils.h"

////////////////////////////////////////////////////////////////////////////////

SetLedStateCommand::SetLedStateCommand(Led &led)
	: BaseLedCommand(led)
{

}

////////////////////////////////////////////////////////////////////////////////

bool SetLedStateCommand::IsMatched(const std::string &sData) const
{
	bool result = std::regex_match(sData, std::regex("set-led-state (on|off)\n"));

	LOGD << METHOD_NAME << " result: " << result;
	return result;
}

////////////////////////////////////////////////////////////////////////////////

std::string SetLedStateCommand::Execute(const std::string &sData)
{
	std::istringstream iss(sData);

	std::string sCommandName, sLedState;
	iss >> sCommandName >> sLedState;

	const std::string &sResult = Utils::BooleanStatusToString(m_led.SetState(Utils::LedStateFromString(sLedState)));

	LOGD << METHOD_NAME << " name: " << sCommandName << ", state: " << sLedState << ", result: " << sResult;
	return sResult;
}

////////////////////////////////////////////////////////////////////////////////
