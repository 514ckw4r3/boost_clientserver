#pragma once

#include <string>

class IConnection
{
public:
	virtual ~IConnection() {}
	virtual void Start() = 0;
	virtual void Stop() = 0;
	virtual void Write(const std::string &sData) = 0;
};

typedef std::shared_ptr<IConnection> ConnectionPtr;
