#include "LedContext.h"

#include "Logger.h"
#include "Utils.h"
#include "SetLedStateCommand.h"
#include "GetLedStateCommand.h"
#include "SetLedColorCommand.h"
#include "GetLedColorCommand.h"
#include "SetLedRateCommand.h"
#include "GetLedRateCommand.h"

////////////////////////////////////////////////////////////////////////////////

LedContext::LedContext()
{
	LOGD << METHOD_NAME;

	InitCommands();
}

////////////////////////////////////////////////////////////////////////////////

void LedContext::HandleData(const std::string &sData, ConnectionPtr pConnection)
{
	LOGD << METHOD_NAME;

	bool bExecuted = false;

	for (auto &pCommand : m_commands)
	{
		if (pCommand->IsMatched(sData))
		{
			const std::string &sCommandResult = pCommand->Execute(sData);
			pConnection->Write("STATUS " + sCommandResult + '\n');
			bExecuted = true;
			break;
		}
	}

	if (!bExecuted)
	{
		pConnection->Write("STATUS " + Utils::BooleanStatusToString(false) + '\n');
	}
}

////////////////////////////////////////////////////////////////////////////////

void LedContext::InitCommands()
{
	LOGD << METHOD_NAME;
	
	m_commands.emplace_back(std::make_shared<SetLedStateCommand>(m_led));
	m_commands.emplace_back(std::make_shared<GetLedStateCommand>(m_led));
	m_commands.emplace_back(std::make_shared<SetLedColorCommand>(m_led));
	m_commands.emplace_back(std::make_shared<GetLedColorCommand>(m_led));
	m_commands.emplace_back(std::make_shared<GetLedRateCommand>(m_led));
	m_commands.emplace_back(std::make_shared<SetLedRateCommand>(m_led));
}

////////////////////////////////////////////////////////////////////////////////
