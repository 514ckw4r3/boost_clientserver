#pragma once

#include "BaseLedCommand.h"

class GetLedStateCommand : public BaseLedCommand
{
public:
	explicit GetLedStateCommand(Led &led);

	virtual bool IsMatched(const std::string &sData) const override;
	virtual std::string Execute(const std::string &sData) override;
};