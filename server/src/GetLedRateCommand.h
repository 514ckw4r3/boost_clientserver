#pragma once

#include "BaseLedCommand.h"

class GetLedRateCommand : public BaseLedCommand
{
public:
	explicit GetLedRateCommand(Led &led);

	virtual bool IsMatched(const std::string &sData) const override;
	virtual std::string Execute(const std::string &sData) override;
};