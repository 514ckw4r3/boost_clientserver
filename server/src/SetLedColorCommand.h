#pragma once

#include "BaseLedCommand.h"

class SetLedColorCommand : public BaseLedCommand
{
public:
	explicit SetLedColorCommand(Led &led);

	virtual bool IsMatched(const std::string &sData) const override;
	virtual std::string Execute(const std::string &sData) override;
};