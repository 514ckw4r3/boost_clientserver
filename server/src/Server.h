#pragma once

#include <boost/asio.hpp>
#include <boost/noncopyable.hpp>
#include <string>
#include "ConnectionManager.h"
#include "IContext.h"

class Server : boost::noncopyable
{
public:
	explicit Server(const std::string &address, const std::string &port);
	void Run();

private:
	void ListenNextConnection();
	void OnSocketConnected(boost::system::error_code ec, boost::asio::ip::tcp::socket socket);
	void CloseAllConnections(boost::system::error_code ec, int signo);
	void DoAwaitStopSignals();

	boost::asio::io_context m_iocontext;
	boost::asio::signal_set m_signals;
	boost::asio::ip::tcp::acceptor m_acceptor;

	ConnectionManagerPtr m_pConnectionManager;
	ContextPtr m_pContext;
};
