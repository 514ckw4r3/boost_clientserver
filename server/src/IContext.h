#pragma once

#include <string>
#include <memory>

#include "IConnection.h"

class IContext
{
public:
	virtual ~IContext() {}
	virtual void HandleData(const std::string &sData, ConnectionPtr pConnection) = 0;
};

typedef std::shared_ptr<IContext> ContextPtr;
