#pragma once

#include <array>
#include <memory>
#include <boost/asio.hpp>
#include <boost/noncopyable.hpp>
#include <unordered_set>
#include <functional>
#include "IConnection.h"
#include "IContext.h"

class Connection;
typedef std::function<void(std::shared_ptr<Connection>)> CloseConnectionCallback;

class Connection : public IConnection, boost::noncopyable, public std::enable_shared_from_this<Connection>
{
public:
	explicit Connection(boost::asio::ip::tcp::socket socket, const ContextPtr &pContext, const CloseConnectionCallback &callback);

	virtual void Start() override;
	virtual void Stop() override;
	virtual void Write(const std::string &sData) override;

private:
	void DoRead();
	void OnDataReceived(boost::system::error_code errorCode, std::size_t nBytesTransferred, const std::shared_ptr<Connection> &pThis);
	bool DataIsValid(const std::string &sData) const;

	std::array<char, 1024> m_buffer;
	boost::asio::ip::tcp::socket m_socket;

	ContextPtr m_pContext;
	CloseConnectionCallback m_closeConnectionCallback;
};
