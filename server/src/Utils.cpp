#include "Utils.h"
#include <cassert>

////////////////////////////////////////////////////////////////////////////////

Led::State Utils::LedStateFromString(const std::string &sParam)
{
	if (sParam == "on")
	{
		return Led::State::On;
	}
	if (sParam == "off")
	{
		return Led::State::Off;
	}
	assert(false);
}

////////////////////////////////////////////////////////////////////////////////

Led::Color Utils::LedColorFromString(const std::string &sParam)
{
	if (sParam == "red")
	{
		return Led::Color::Red;
	}
	if (sParam == "green")
	{
		return Led::Color::Green;
	}
	if (sParam == "blue")
	{
		return Led::Color::Blue;
	}
	assert(false);
}


////////////////////////////////////////////////////////////////////////////////

std::string Utils::LedStateToString(Led::State state)
{
	switch (state)
	{
	case Led::State::On:
		return "on";
	case Led::State::Off:
		return "off";
	default:
		assert(false);
	}
}

////////////////////////////////////////////////////////////////////////////////

std::string Utils::LedColorToString(Led::Color color)
{
	switch (color)
	{
	case Led::Color::Red:
		return "red";
	case Led::Color::Green:
		return "green";
	case Led::Color::Blue:
		return "blue";
	default:
		assert(false);
	}
}

////////////////////////////////////////////////////////////////////////////////

std::string Utils::BooleanStatusToString(bool result)
{
	return result ? "OK" : "FAILED";
}

////////////////////////////////////////////////////////////////////////////////
