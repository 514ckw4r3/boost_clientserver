#pragma once

#include "BaseLedCommand.h"

class GetLedColorCommand : public BaseLedCommand
{
public:
	explicit GetLedColorCommand(Led &led);

	virtual bool IsMatched(const std::string &sData) const override;
	virtual std::string Execute(const std::string &sData) override;
};