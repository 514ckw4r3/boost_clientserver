#include "SetLedColorCommand.h"

#include <regex>
#include <sstream>
#include "Logger.h"
#include "Led.h"
#include "Utils.h"

////////////////////////////////////////////////////////////////////////////////

SetLedColorCommand::SetLedColorCommand(Led &led)
	: BaseLedCommand(led)
{

}

////////////////////////////////////////////////////////////////////////////////

bool SetLedColorCommand::IsMatched(const std::string &sData) const
{
	bool result = std::regex_match(sData, std::regex("set-led-color (red|green|blue)\n"));

	LOGD << METHOD_NAME << " result: " << result;
	return result;
}

////////////////////////////////////////////////////////////////////////////////

std::string SetLedColorCommand::Execute(const std::string &sData)
{
	std::istringstream iss(sData);

	std::string sCommandName, sLedColor;
	iss >> sCommandName >> sLedColor;

	const std::string &sResult = Utils::BooleanStatusToString(m_led.SetColor(Utils::LedColorFromString(sLedColor)));

	LOGD << METHOD_NAME << " name: " << sCommandName << ", state: " << sLedColor << ", result: " << sResult;
	return sResult;
}

////////////////////////////////////////////////////////////////////////////////
