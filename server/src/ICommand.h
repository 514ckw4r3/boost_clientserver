#pragma once

#include <memory>
#include <string>

class ICommand
{
public:
	virtual ~ICommand() {}
	virtual bool IsMatched(const std::string &sData) const = 0;
	virtual std::string Execute(const std::string &sData) = 0;
};

typedef std::shared_ptr<ICommand> CommandPtr;