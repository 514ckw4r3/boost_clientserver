#pragma once

#include "Led.h"

namespace Utils
{
	Led::State LedStateFromString(const std::string &sParam);
	Led::Color LedColorFromString(const std::string &sParam);

	std::string LedStateToString(Led::State state);
	std::string LedColorToString(Led::Color color);

	std::string BooleanStatusToString(bool result);
}