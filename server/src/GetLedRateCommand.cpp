#include "GetLedRateCommand.h"

#include <regex>
#include <sstream>
#include "Logger.h"
#include "Led.h"
#include "Utils.h"

////////////////////////////////////////////////////////////////////////////////

GetLedRateCommand::GetLedRateCommand(Led &led)
	: BaseLedCommand(led)
{

}

////////////////////////////////////////////////////////////////////////////////

bool GetLedRateCommand::IsMatched(const std::string &sData) const
{
	bool result = sData == "get-led-rate\n" ? true : false;

	LOGD << METHOD_NAME << " result: " << result;
	return result;
}

////////////////////////////////////////////////////////////////////////////////

std::string GetLedRateCommand::Execute(const std::string &sData)
{
	std::string sResult = "OK " + std::to_string(m_led.GetRate());

	LOGD << METHOD_NAME << " result: " << sResult;
	return sResult;
}

////////////////////////////////////////////////////////////////////////////////