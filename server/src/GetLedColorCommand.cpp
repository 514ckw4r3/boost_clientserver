#include "GetLedColorCommand.h"

#include <regex>
#include <sstream>
#include "Logger.h"
#include "Led.h"
#include "Utils.h"

////////////////////////////////////////////////////////////////////////////////

GetLedColorCommand::GetLedColorCommand(Led &led)
	: BaseLedCommand(led)
{

}

////////////////////////////////////////////////////////////////////////////////

bool GetLedColorCommand::IsMatched(const std::string &sData) const
{
	bool result = sData == "get-led-color\n" ? true : false;

	LOGD << METHOD_NAME << " result: " << result;
	return result;
}

////////////////////////////////////////////////////////////////////////////////

std::string GetLedColorCommand::Execute(const std::string &sData)
{
	std::string sResult = "OK " + Utils::LedColorToString(m_led.GetColor());

	LOGD << METHOD_NAME << " result: " << sResult;
	return sResult;
}

////////////////////////////////////////////////////////////////////////////////