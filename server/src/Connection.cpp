#include "Connection.h"
#include <utility>
#include <vector>
#include <functional>
#include "Logger.h"
#include "ICommand.h"

#define METHOD_NAME __FUNCTION__"() [" << this << ']'

////////////////////////////////////////////////////////////////////////////////

Connection::Connection(boost::asio::ip::tcp::socket socket, const ContextPtr &pContext, const CloseConnectionCallback &callback)
	: m_socket(std::move(socket))
	, m_pContext(pContext)
	, m_closeConnectionCallback(callback)
{
	LOGD << METHOD_NAME;
}

////////////////////////////////////////////////////////////////////////////////

void Connection::Start()
{
	LOGD << METHOD_NAME;

	DoRead();
}

////////////////////////////////////////////////////////////////////////////////

void Connection::Stop()
{
	LOGD << METHOD_NAME;

	boost::system::error_code ec;
	m_socket.close(ec);
}

////////////////////////////////////////////////////////////////////////////////

void Connection::Write(const std::string &sData)
{
	LOGD << METHOD_NAME << ", data: " << sData;

	std::copy(sData.begin(), sData.end(), m_buffer.data());

	auto pThis(shared_from_this());
	boost::asio::async_write(m_socket, boost::asio::buffer(m_buffer, sData.size()), [this, pThis](boost::system::error_code errorCode, std::size_t)
	{
		if (errorCode && errorCode != boost::asio::error::operation_aborted)
		{
			m_closeConnectionCallback(pThis);
		}
		else
		{
			DoRead();
		}
	});
}

////////////////////////////////////////////////////////////////////////////////

void Connection::DoRead()
{
	LOGD << METHOD_NAME;

	auto pThis(shared_from_this());
	m_socket.async_read_some(boost::asio::buffer(m_buffer), std::bind(&Connection::OnDataReceived, this, std::placeholders::_1, std::placeholders::_2, pThis));
}

////////////////////////////////////////////////////////////////////////////////

void Connection::OnDataReceived(boost::system::error_code errorCode, std::size_t nBytesTransferred, const std::shared_ptr<Connection> &pThis)
{
	LOGD << METHOD_NAME << ", error code: " << errorCode << ", bytes: " << nBytesTransferred;

	std::string sData(m_buffer.begin(), m_buffer.begin() + nBytesTransferred);

	LOGD << METHOD_NAME << " data : " << sData;

	if (errorCode && errorCode != boost::asio::error::operation_aborted || !DataIsValid(sData))
	{
		m_closeConnectionCallback(pThis);
	}
	else
	{
		m_pContext->HandleData(sData, pThis);
	}
}

////////////////////////////////////////////////////////////////////////////////

bool Connection::DataIsValid(const std::string &sData) const
{
	bool result = !sData.empty() && sData.back() == '\n';
	LOGD << METHOD_NAME << " result: " << result;
	return result;
}

////////////////////////////////////////////////////////////////////////////////
