#pragma once

#include <boost/log/trivial.hpp>

#define METHOD_NAME BOOST_CURRENT_FUNCTION

#define LOGD BOOST_LOG_TRIVIAL(debug)
#define LOGE BOOST_LOG_TRIVIAL(error)