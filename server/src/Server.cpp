#include "Server.h"

#include <csignal>
#include <functional>
#include "Connection.h"
#include "Logger.h"
#include "LedContext.h"

////////////////////////////////////////////////////////////////////////////////

Server::Server(const std::string &address, const std::string &port)
	: m_iocontext(1)
	, m_signals(m_iocontext)
	, m_acceptor(m_iocontext)
	, m_pConnectionManager(std::make_shared<ConnectionManager>())
	, m_pContext(std::make_shared<LedContext>())
{
	LOGD << METHOD_NAME << ": address: " << address << ", port: " << port;

	DoAwaitStopSignals();

	boost::asio::ip::tcp::resolver resolver(m_iocontext);
	boost::asio::ip::tcp::endpoint endpoint = *resolver.resolve(address, port).begin();
	m_acceptor.open(endpoint.protocol());
	m_acceptor.set_option(boost::asio::ip::tcp::acceptor::reuse_address(true));
	m_acceptor.bind(endpoint);
	m_acceptor.listen();
}

////////////////////////////////////////////////////////////////////////////////

void Server::Run()
{
	LOGD << METHOD_NAME;

	ListenNextConnection();
	m_iocontext.run();
}

////////////////////////////////////////////////////////////////////////////////

void Server::ListenNextConnection()
{
	LOGD << METHOD_NAME;

	m_acceptor.async_accept(std::bind(&Server::OnSocketConnected, this, std::placeholders::_1, std::placeholders::_2));
}

////////////////////////////////////////////////////////////////////////////////

void Server::OnSocketConnected(boost::system::error_code ec, boost::asio::ip::tcp::socket socket)
{
	LOGD << METHOD_NAME << ": error code: " << ec;

	if (!m_acceptor.is_open())
	{
		return;
	}

	if (!ec)
	{
		m_pConnectionManager->Start(std::make_shared<Connection>(std::move(socket), m_pContext, [this](ConnectionPtr pConnection) { m_pConnectionManager->Stop(pConnection); }));
	}

	ListenNextConnection();
}

////////////////////////////////////////////////////////////////////////////////

void Server::CloseAllConnections(boost::system::error_code ec, int signo)
{
	LOGD << METHOD_NAME << ": error code: " << ec << ", signo: " << signo;

	m_acceptor.close();
	m_pConnectionManager->StopAll();
}

////////////////////////////////////////////////////////////////////////////////

void Server::DoAwaitStopSignals()
{
	LOGD << METHOD_NAME;

	m_signals.add(SIGINT);
	m_signals.add(SIGTERM);
	m_signals.add(SIGBREAK);
#if defined(SIGQUIT)
	m_signals.add(SIGQUIT);
#endif

	m_signals.async_wait(std::bind(&Server::CloseAllConnections, this, std::placeholders::_1, std::placeholders::_2));
}

////////////////////////////////////////////////////////////////////////////////
