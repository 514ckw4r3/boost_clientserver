#pragma once

#include "BaseLedCommand.h"

class SetLedStateCommand : public BaseLedCommand
{
public:
	explicit SetLedStateCommand(Led &led);

	virtual bool IsMatched(const std::string &sData) const override;
	virtual std::string Execute(const std::string &sData) override;
};