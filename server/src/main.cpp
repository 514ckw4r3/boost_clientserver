#include "server.h"

int main(int argc, char **argv)
{
	std::string host = "0.0.0.0";
	std::string port = "80";

	if (argc == 3)
	{
		host = argv[1];
		port = argv[2];
	}

	Server server(host, port);
	server.Run();
}