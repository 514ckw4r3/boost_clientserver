#include "GetLedStateCommand.h"

#include <regex>
#include <sstream>
#include "Logger.h"
#include "Led.h"
#include "Utils.h"

////////////////////////////////////////////////////////////////////////////////

GetLedStateCommand::GetLedStateCommand(Led &led)
	: BaseLedCommand(led)
{

}

////////////////////////////////////////////////////////////////////////////////

bool GetLedStateCommand::IsMatched(const std::string &sData) const
{
	bool result = sData == "get-led-state\n" ? true : false;

	LOGD << METHOD_NAME << " result: " << result;
	return result;
}

////////////////////////////////////////////////////////////////////////////////

std::string GetLedStateCommand::Execute(const std::string &sData)
{
	std::string sResult = "OK " + Utils::LedStateToString(m_led.GetState());

	LOGD << METHOD_NAME << " result: " << sResult;
	return sResult;
}

////////////////////////////////////////////////////////////////////////////////