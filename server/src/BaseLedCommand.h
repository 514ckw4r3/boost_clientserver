#pragma once

#include "ICommand.h"
#include "Led.h"

class BaseLedCommand : public ICommand
{
public:
	explicit BaseLedCommand(Led &led);

protected:
	Led &m_led;
};
