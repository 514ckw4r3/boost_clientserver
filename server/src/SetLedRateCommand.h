#pragma once

#include "BaseLedCommand.h"

class SetLedRateCommand : public BaseLedCommand
{
public:
	explicit SetLedRateCommand(Led &led);

	virtual bool IsMatched(const std::string &sData) const override;
	virtual std::string Execute(const std::string &sData) override;
};