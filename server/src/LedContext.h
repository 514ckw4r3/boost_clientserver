#pragma once

#include <vector>
#include "IContext.h"
#include "ICommand.h"
#include "Led.h"

typedef std::vector<std::shared_ptr<ICommand>> LedCommandVector;

class LedContext : public IContext
{
public:
	explicit LedContext();
	virtual void HandleData(const std::string &sData, ConnectionPtr pConnection) override;

private:
	void InitCommands();

	Led m_led;
	LedCommandVector m_commands;
};
