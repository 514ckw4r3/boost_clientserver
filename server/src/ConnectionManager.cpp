#include "ConnectionManager.h"
#include "Logger.h"

////////////////////////////////////////////////////////////////////////////////

void ConnectionManager::Start(const ConnectionPtr &pConnection)
{
	LOGD << METHOD_NAME;

	m_connections.insert(pConnection);
	pConnection->Start();
}

////////////////////////////////////////////////////////////////////////////////

void ConnectionManager::Stop(const ConnectionPtr &pConnection)
{
	LOGD << METHOD_NAME;

	m_connections.erase(pConnection);
	pConnection->Stop();
}

////////////////////////////////////////////////////////////////////////////////

void ConnectionManager::StopAll()
{
	LOGD << METHOD_NAME;

	for (auto pConnection : m_connections)
	{
		pConnection->Stop();
	}
	m_connections.clear();
}

////////////////////////////////////////////////////////////////////////////////
