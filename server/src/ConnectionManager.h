#pragma once

#include <unordered_set>
#include <memory>
#include <boost/noncopyable.hpp>
#include "Connection.h"

class ConnectionManager : boost::noncopyable
{
public:
	void Start(const ConnectionPtr &pConnection);
	void Stop(const ConnectionPtr &pConnection);
	void StopAll();

private:
	typedef std::unordered_set<ConnectionPtr> ConnectionSet;

	ConnectionSet m_connections;
};

typedef std::shared_ptr<ConnectionManager> ConnectionManagerPtr;
