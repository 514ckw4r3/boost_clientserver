#include "SetLedRateCommand.h"

#include <regex>
#include <sstream>
#include "Logger.h"
#include "Led.h"
#include "Utils.h"

////////////////////////////////////////////////////////////////////////////////

SetLedRateCommand::SetLedRateCommand(Led &led)
	: BaseLedCommand(led)
{

}

////////////////////////////////////////////////////////////////////////////////

bool SetLedRateCommand::IsMatched(const std::string &sData) const
{
	bool result = std::regex_match(sData, std::regex("set-led-rate [0-5]\n"));

	LOGD << METHOD_NAME << " result: " << result;
	return result;
}

////////////////////////////////////////////////////////////////////////////////

std::string SetLedRateCommand::Execute(const std::string &sData)
{
	std::istringstream iss(sData);

	std::string sCommandName;
	int rate;
	iss >> sCommandName >> rate;

	const std::string &sResult = Utils::BooleanStatusToString(m_led.SetRate(rate));

	LOGD << METHOD_NAME << " name: " << sCommandName << ", rate: " << rate << ", result: " << sResult;
	return sResult;
}

////////////////////////////////////////////////////////////////////////////////
