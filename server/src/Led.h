#pragma once

#include <memory>
#include <string>

class Led
{
public:
	enum class State { On, Off };
	enum class Color { Red, Green, Blue };

	explicit Led();

	bool SetState(State state);
	State GetState() const;

	bool SetColor(Color color);
	Color GetColor() const;

	bool SetRate(int rate);
	int GetRate() const;

private:
	State m_state;
	Color m_color;
	int m_rate;
};
